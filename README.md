# Fundamentos de los Sistemas Inteligentes #

### Práctica 1 - Primera parte ###

La estrategia de búsqueda de ramificación y acotación pertenece a las estrategias de búsqueda no informada. Su principio de funcionamiento se basa en ordenar la lista abierta tomando como criterio el coste acumulado de cada trayectoria parcial, representada ésta por cada nodo de la lista abierta. Por tanto, el primer nodo de la lista irá expandiéndose y generando nuevas trayectorias por ramificación de sus hijos.

### Tareas ###

* A partir del código base entregado, se deberá programar el método de ramificación y acotación. Utilícese como problema el grafo de las ciudades de Rumanía presente en 
el código.
* Comparar la cantidad de nodos expandidos por este método con relación a los métodos de búsqueda primero en anchura y primero en profundidad.
* Realizar a mano la traza de una búsqueda.