# -*- coding: utf-8 -*-

# Search methods
import search

## Arad to Bucharest ##
# Ramificacion y acotacion: 131 (52 expansiones)
# FIFO: 59 (21 expansiones)
# STACK: 23 (tras quitar la lista cerrada hace un bucle infinito)
ab = search.GPSProblem('A', 'B', search.romania)

## Oradea to Giurgiu ##
# Ramificacion y acotacion: 299 (111 expansiones)
# FIFO: 148 (54 expansiones)
# STACK: 31 (tras quitar la lista cerrada hace un bucle infinito)
og = search.GPSProblem('O', 'G', search.romania)

## Craiova to Fagaras ##
# Ramificacion y acotacion: 50 (17 expansiones)
# FIFO: 61 (21 expansiones)
# STACK: 22 (tras quitar la lista cerrada hace un bucle infinito)
cf = search.GPSProblem('C', 'F', search.romania)

#print search.breadth_first_graph_search(ab).path() # FIFO (primero en anchura)
#print search.depth_first_graph_search(ab).path() # STACK (primero en profundidad)
#print search.iterative_deepening_search(ab).path()
#print search.depth_limited_search(ab).path()

print ""
print " --------------------------------------------- "
print " ------------------> Traza <------------------ "
print " --------------------------------------------- "
print ""

result = search.busqueda_ramificacion_acotacion(ab).path()[::-1]

print ""
print " --------------------------------------------- "
print " ----------------> Resultado <---------------- "
print " --------------------------------------------- "
print ""

print "Para ir del " + str(result[0]) + " al " + str(result[len(result)-1]) + " el mejor camino es:"
print ""
print str(result) + " : " + str(result[len(result)-1].path_cost) + "KM"

# Result:
# [<Node B>, <Node P>, <Node R>, <Node S>, <Node A>] : 101 + 97 + 80 + 140 = 418
# [<Node B>, <Node F>, <Node S>, <Node A>] : 211 + 99 + 140 = 450

# Informacion del algoritmo: "Lectura 6 - Búsqueda informada pag. 52"
# Tener en cuenta que se puede volver al nodo padre desde el hijo, asi que cambia un poco respecto al ejercicio de la teoria

# Realizado por Pablo José Díaz García y Dilma Sae
